#![feature(await_macro, async_await)]

use futures::{
	prelude::{SinkExt, StreamExt},
	channel::mpsc,
	executor::ThreadPool,
	task::SpawnExt,
};

use libloading as lib;

async fn call_dynamic(exec: ThreadPool) -> lib::Result<()> {
	let lib = lib::Library::new("./target/debug/libplugin.so")?;
	unsafe {
		let func2: lib::Symbol<unsafe fn(ThreadPool)> = lib.get(b"func2")?;
		func2(exec.clone());
	};
	let mut receiver = unsafe {
		let func: lib::Symbol<unsafe fn(ThreadPool) -> mpsc::UnboundedReceiver<u32>> =
			lib.get(b"func")?;
		func(exec)
	};
	while let Some(res) = await!(receiver.next()) {
		println!("dynamic {:?}", res);
	}
	Ok(())
}

fn main() {
	let mut executor = ThreadPool::new()
		.expect("Failed to initialize threapooled executor");

	let fut = {
		let executor = executor.clone();
		async move {
			await!(whereami());
			await!(call_local(executor.clone()));
			await!(call_dynamic(executor)).unwrap();
		}
	};

	executor.run(fut);
	println!("Complete");
}

async fn call_local(exec: ThreadPool) {
	let mut receiver = local(exec);
	while let Some(res) = await!(receiver.next()) {
		println!("local {:?}", res);
	}
}

pub fn local(mut exec: ThreadPool) -> mpsc::UnboundedReceiver<u32> {
	let (mut sender, receiver) = mpsc::unbounded::<u32>();
	exec.spawn(async move {
		await!(sender.send(42)).unwrap();
		await!(sender.send(43)).unwrap();
		await!(sender.send(44)).unwrap();
	})
	.expect("Failed to spawn");
	receiver
}

async fn whereami() {
	println!("app");
}
