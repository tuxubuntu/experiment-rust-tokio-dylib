#![feature(await_macro, async_await)]

use futures::{
	prelude::SinkExt,
	channel::mpsc,
	executor::ThreadPool,
	task::SpawnExt,
};

async fn whereami() {
	println!("plugin");
}

#[no_mangle]
pub fn func(mut exec: ThreadPool) -> mpsc::UnboundedReceiver<u32> {
	let (mut sender, receiver) = mpsc::unbounded::<u32>();

	exec.spawn(async move {
		await!(whereami());
		await!(sender.send(42)).unwrap();
		await!(sender.send(43)).unwrap();
		await!(sender.send(44)).unwrap();
	})
	.expect("Failed to spawn");

	receiver
}

#[no_mangle]
pub fn func2(mut exec: ThreadPool) {
	exec.spawn(async {
		await!(whereami());
	})
	.expect("Failed to spawn");
}

#[cfg(test)]
mod tests {
	#[test]
	fn it_works() {
		assert_eq!(2 + 2, 4);
	}
}
